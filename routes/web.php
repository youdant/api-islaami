<?php /** @noinspection PhpUndefinedVariableInspection */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('', function () use ($router) {
    return $router->app->version();
});

/*$router->get('key', function () {
    return Str::random(32);
});*/

$router->group(['prefix' => 'api'], function () use ($router) {
    /* ADMIN */
    $router->group(['prefix' => 'panel'], function () use ($router) {
        $router->post('login', 'AuthController@loginAdmin');
    });

    $router->group(['prefix' => 'panel', 'middleware' => ['auth', 'admin']], function () use ($router) {
        // AUTH
        $router->post('logout', 'AuthController@logoutAdmin');
        $router->patch('{id}/password', ['uses' => 'UserController@changeAdminPassword']);

        // USERS
        $router->group(['prefix' => 'users'], function () use ($router) {
            $router->get('', ['uses' => 'UserController@index']);
            $router->patch('{id}', ['uses' => 'UserController@changeUserSuspend']);
        });

        // VIDEO
        $router->group(['prefix' => 'videos'], function () use ($router) {
            $router->get('', ['uses' => 'VideoController@index']);
            $router->post('', ['uses' => 'VideoController@createVideo']);
            $router->put('{id}', ['uses' => 'VideoController@updateVideo']);
            $router->patch('{id}', ['uses' => 'VideoController@changeStatus']);
            $router->delete('{id}', ['uses' => 'VideoController@deleteVideo']);
        });

        // ARTICLE
        $router->group(['prefix' => 'articles'], function () use ($router) {
            $router->get('', ['uses' => 'ArticleController@index']);
            $router->post('', ['uses' => 'ArticleController@createArticle']);
            $router->get('{id}', ['uses' => 'ArticleController@getArticle']);
            $router->put('{id}', ['uses' => 'ArticleController@editArticle']);
            $router->delete('{id}', ['uses' => 'ArticleController@deleteArticle']);
        });

        $router->group(['prefix' => 'channels'], function () use ($router) {
            $router->get('', ['uses' => 'ChannelController@index']);
            $router->post('create', ['uses' => 'ChannelController@createChannel']);
        });
    });
    /* END OF ADMIN */

    // testing
    $router->post('email', 'AuthController@sendEmail');

    // AUTH
    $router->post('register', 'AuthController@register');
    $router->patch('verifyuser', 'AuthController@verifyUser');
    $router->patch('resendcode', 'AuthController@resendVerificationCode');
    $router->post('login', 'AuthController@login');
    $router->post('login/google', 'AuthController@loginByGoogle');

    // ARTICLES
    $router->group(['prefix' => 'articles', 'middleware' => ['auth']], function () use ($router) {
        $router->get('', ['uses' => 'ArticleController@index']);
        $router->get('{id}', ['uses' => 'ArticleController@getArticle']);
    });

    // USERS
    $router->group(['prefix' => 'users', 'middleware' => ['auth']], function () use ($router) {
        $router->get('videos', ['uses' => 'VideoController@getVideoFollow']);
        $router->get('profile', ['uses' => 'UserController@getUser']);
        $router->put('{id}', ['uses' => 'UserController@updateUser']);
        $router->post('logout', 'AuthController@logout');
    });

    // VIDEOS
    $router->group(['prefix' => 'videos', 'middleware' => ['auth']], function () use ($router) {
        $router->get('', ['uses' => 'VideoController@index']);
        $router->get('follow', ['uses' => 'VideoController@getVideoFollow']);
        $router->get('{id}', ['uses' => 'VideoController@getVideo']);
        $router->post('{id}/add', ['uses' => 'PlaylistController@addToPlaylist']);
        $router->delete('{id}/remove', ['uses' => 'PlaylistController@removeFromPlaylist']);
        $router->post('{id}/later', ['uses' => 'PlaylistController@addToWatchLater']);
        $router->delete('{id}/later', ['uses' => 'PlaylistController@deleteFromWatchLater']);
    });

    // PLAYLIST
    $router->group(['prefix' => 'playlists', 'middleware' => ['auth']], function () use ($router) {
        $router->get('', ['uses' => 'PlaylistController@index']);
        $router->get('later', ['uses' => 'PlaylistController@watchLater']);
        $router->get('{id}', ['uses' => 'PlaylistController@getPlaylist']);
        $router->post('create', ['uses' => 'PlaylistController@createPlaylist']);
        $router->patch('{id}', ['uses' => 'PlaylistController@changePlaylistName']);
        $router->delete('{id}', ['uses' => 'PlaylistController@deletePlaylist']);
    });

    // CHANNEL
    $router->group(['prefix' => 'channels', 'middleware' => ['auth']], function () use ($router) {
        $router->get('{id}/detail', ['uses' => 'ChannelController@getChannelDetail']);
        $router->get('{id}/videos', ['uses' => 'ChannelController@getChannelVideos']);
        $router->get('follow', ['uses' => 'ChannelController@getChannelFollow']);
        $router->get('hide', ['uses' => 'ChannelController@getChannelHide']);
        $router->get('{id}/follow/status', ['uses' => 'ChannelController@getFollowingStatus']);
        $router->get('{id}/hide/status', ['uses' => 'ChannelController@getHideStatus']);
        $router->post('{id}/hide', ['uses' => 'ChannelController@hideChannel']);
        $router->post('{id}/follow', ['uses' => 'ChannelController@followChannel']);
        $router->delete('{id}/show', ['uses' => 'ChannelController@showChannel']);
        $router->delete('{id}/unfollow', ['uses' => 'ChannelController@unfollowChannel']);
    });

    // CATEGORY & SUBCATEGORY
    $router->post('article/category', ['uses' => 'ArticleController@createCategory']);
    $router->get('articles/category/{id}', ['uses' => 'ArticleController@getAllArticlesByCategoryID']);
    $router->get('categories', ['uses' => 'CategoryController@index']);
    $router->post('category', ['uses' => 'CategoryController@createCategory']);
    $router->get('subcategories', ['uses' => 'CategoryController@subcategoryIndex']);
    $router->post('subcategory', ['uses' => 'CategoryController@createSubcategory']);
    $router->get('labels', ['uses' => 'CategoryController@labelIndex']);

    // USER REPORTS, INSIGHT/CRITICS, AND RECOMMENDATIONS
    $router->group(['prefix' => 'extras', 'middleware' => ['auth']], function () use ($router) {
        $router->post('recommendation', ['uses' => 'ReportController@createRecommendation']);
        $router->post('insight', ['uses' => 'ReportController@createInsight']);
        $router->post('report', ['uses' => 'ReportController@createReport']);
    });
    $router->get('reports', ['uses' => 'ReportController@index']);
    $router->get('insights', ['uses' => 'ReportController@indexInsight']);
    $router->get('recommendations', ['uses' => 'ReportController@indexRecommendation']);
    $router->patch('report/{id}', ['uses' => 'ReportController@changeReportStatus']);
});
