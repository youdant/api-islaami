<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullname' => "John Doe",
            'email' => "hello@mail.com",
            'birthdate' => "1996-03-22",
            'gender' => "L",
            'password' => Hash::make("password"),
            'is_verified' => true,
        ]);

        DB::table('users')->insert([
            'fullname' => "John Doe 1",
            'email' => "hello1@mail.com",
            'birthdate' => "1996-03-22",
            'gender' => "L",
            'password' => Hash::make("test"),
            'is_verified' => true,
        ]);
    }
}
