<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('channels')->insert([
            'name' => "Calon Sarjana 1",
            'channel_thumbnail' => "https://yt3.ggpht.com/a/AGF-l78XSVAVImM1kvNSf1S5b-JA5p7qzcGse1ergw=s288-c-k-c0xffffffff-no-rj-mo",
            'channel_bio' => "New Video Everyday",
        ]);

        DB::table('channels')->insert([
            'name' => "Calon Sarjana 2",
            'channel_thumbnail' => "https://yt3.ggpht.com/a/AGF-l78XSVAVImM1kvNSf1S5b-JA5p7qzcGse1ergw=s288-c-k-c0xffffffff-no-rj-mo",
            'channel_bio' => "New Video Everyday",
        ]);

        DB::table('categories')->insert([
            ['name' => "Pengetahuan", 'order_number' => 1],
            ['name' => "Kajian", 'order_number' => 2],
        ]);

        DB::table('subcategories')->insert([
            ['name' => "Funfact", 'category_id' => 1],
            ['name' => "Remaja", 'category_id' => 2],
            ['name' => "Trivial", 'category_id' => 1],
        ]);

        DB::table('videos')->insert([
            'title' => "BISA JAWAB SEMUA BERARTI JENIUS!🔥 TEKA-TEKI HITUNG-HITUNGAN YANG BIKIN KEPALA KALIAN NGEBUL!🔥",
            'video_url' => "https://www.youtube.com/watch?v=IRB0J5BdvlA",
            'video_thumbnail' => "https://img.youtube.com/vi/IRB0J5BdvlA/hqdefault.jpg",
            'description' => "<p>Lorem ipsum dolor sit amet.</p><p><a href=\"http://www.google.com\">www.google.com</a></p>",
            'channel_id' => 1,
            'category_id' => 1,
            'subcategory_id' => 3,
            'is_published' => true,
            'published_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('videos')->insert([
            'title' => "BISA JAWAB SEMUA BERARTI JENIUS!🔥 TEKA-TEKI HITUNG-HITUNGAN YANG BIKIN KEPALA KALIAN NGEBUL!🔥",
            'video_url' => "https://www.youtube.com/watch?v=IRB0J5BdvlA",
            'video_thumbnail' => "https://img.youtube.com/vi/IRB0J5BdvlA/hqdefault.jpg",
            'description' => "<p>Lorem ipsum dolor sit amet.</p><p><a href=\"http://www.google.com\">www.google.com</a></p>",
            'channel_id' => 2,
            'category_id' => 1,
            'subcategory_id' => 3,
            'is_published' => true,
            'published_at' => Carbon::now()->toDateTimeString()
        ]);

        DB::table('followers')->insert([
            "user_id" => 1,
            "channel_id" => 2
        ]);
    }
}
