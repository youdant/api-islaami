<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->is_admin > 0) {
            $limit = $request->get("limit");
            $filter = $request->get("filter");
            $query = $request->get("query");

            $users = DB::table("users")->where("is_admin", "=", "0");

            if (strlen($filter) == 0) {
                $users->where("is_suspended", 0);
            } else {
                if (strtolower($filter) == "active") $users->where("is_suspended", 0);
                else if (strtolower($filter) == "inactive") $users->where("is_suspended", 1);
            }

            if (strlen($limit) == 0) $limit = 10;

            if (strlen($query) > 0) $users->where("fullname", $query);

            $data = $this->createSuccessJSON($users->paginate($limit), "");
            return response()->json($data, 200);
        } else {
            $data = $this->createFailedResponse("UNAUTHORIZED", "");
            return response()->json($data, 401);
        }
    }

    public function getUser()
    {
        $auth = Auth::user();

        $data = $this->createSuccessJSON($auth, "");
        return response()->json($data, 200);
    }

    public function getUserByID($id)
    {
        $user = User::find($id);

        $data = $this->createSuccessJSON($user, "");
        return response()->json($data, 200);
    }

    public function changeUserSuspend(Request $request, $id)
    {
        $user = User::find($id);
        $user->is_suspended = ($request->is_suspended == 0) ? 0 : 1;
        $user->save();

        $data = $this->createSuccessJSON("", "Berhasil blokir user");
        return response()->json($data, 200);
    }

    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->fullname = $request->fullname;
        $user->email = $request->email;
        $user->birthdate = $request->birthdate;
        $user->gender = $request->gender;
        $user->save();

        $data = $this->createSuccessJSON("", "Berhasil mengupdate profil");
        return response()->json($data, 201);
    }

    public function changeAdminPassword(Request $request, $id)
    {
        $admin = Admin::find($id);
        $newPassword = Hash::make($request->new_password);
        if (Hash::check($request->old_password, $admin->password)) {
            if (!Hash::check($request->new_password, $admin->password)) {
                $userCurrentPassword = $newPassword;
                DB::table('admin')->where('id', $id)->update(['password' => $userCurrentPassword]);
                $data = $this->createSuccessJSON("", "Berhasil ganti password");
            } else {
                $data = $this->createFailedJSON("Password baru Anda sama dengan sebelumnya");
            }
        } else {
            $data = $this->createFailedJSON("Password Anda salah");
        }
        return response()->json($data, 201);
    }
}
