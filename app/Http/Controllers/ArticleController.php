<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\User;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $articles = Article::all();

        $data = $this->createSuccessJSON($articles, "");
        return response()->json($data, 200);
    }

    public function createArticle(Request $request)
    {
        $jsonRequest = json_decode($request->getContent());

        $article = new Article();
        $article->title = $jsonRequest->article_title;
        $article->content = $jsonRequest->article_content;
        $article->category_id = $jsonRequest->category_id;
        $article->save();

        $data = $this->createSuccessJSON($article, "Berhasil Membuat Artikel");
        return response()->json($data, 200);
    }

    public function getArticle($id)
    {
        $article = Article::find($id);

        $data = $this->createSuccessJSON($article, "");
        return response()->json($data, 200);
    }

    public function editArticle(Request $request, $id)
    {
        $jsonRequest = json_decode($request->getContent());

        $article = Article::find($id);
        $article->title = $jsonRequest->article_title;
        $article->content = $jsonRequest->article_content;
        $article->category_id = $jsonRequest->category_id;
        $article->save();

        $data = $this->createSuccessJSON($article, "Berhasil Update Artikel");
        return response()->json($data, 200);
    }

    public function deleteArticle($id)
    {
        Article::destroy($id);

        $data = $this->createSuccessJSON("", "Artikel Berhasil Dihapus");
        return response()->json($data, 200);
    }

    public function createCategory(Request $request)
    {
        $category = new ArticleCategory();
        $category->name = $request->name;
        $category->save();

        $data = $this->createSuccessJSON($category, "Berhasil Membuat Kategori Artikel");
        return response()->json($data, 200);
    }

    public function getAllArticlesByCategoryID($id)
    {
        $articles = Article::where("category_id", $id)->get();

        $data = $this->createSuccessJSON($articles, "");
        return response()->json($data, 200);
    }
}
