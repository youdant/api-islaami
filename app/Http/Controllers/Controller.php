<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

//use Illuminate\Support\Facades\Auth;
class Controller extends BaseController
{
    public function createSuccessJSON($data, $message)
    {
        return [
            'data' => $data,
            'status' => true,
            'message' => 'SUCCESS',
            'userMsg' => $message,
        ];
    }

    public function createFailedJSON($message)
    {
        return [
            'status' => false,
            'message' => 'FAILED',
            'userMsg' => $message,
        ];
    }

    public function createFailedResponse($message, $userMsg)
    {
        return [
            'status' => false,
            'message' => $message,
            'userMsg' => $userMsg,
        ];
    }
}
