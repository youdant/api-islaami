<?php

namespace App\Http\Controllers;

use App\Mail\Email;
use App\User;
use FCM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class AuthController extends Controller
{
    public function loginAdmin(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $user = User::where([
            ["email", $request->email],
            ["is_admin", "=", 1]
        ])->first();
        if ($user != null) {
            if (!$token = Auth::attempt($credentials)) {
                $data = $this->createFailedResponse("WRONG_CREDENTIALS", "Email/Kata Sandi Anda salah");
                return response()->json($data, 401);
            }

            $user->api_token = $token;
            $user->save();
        } else {
            $data = $this->createFailedResponse("EMAIL_NOT_FOUND", "Email " . $request->email . " belum terdaftar");
            return response()->json($data, 401);
        }

        $data = $this->createSuccessJSON($user, "");
        return response()->json($data, 200);
    }

    public function logoutAdmin()
    {
        $auth = Auth::user();

        Auth::logout();
        $user = User::where([
            ["email", $auth->email],
            ["is_admin", "=", 1]
        ])->first();
        $user->api_token = null;
        $user->save();

        $data = $this->createSuccessJSON(null, "Berhasil Logout");
        return response()->json($data, 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        $user = User::where("email", $request->email)->first();
        if ($user != null) {
            if ($user->is_verified == 0) {
                $data = $this->createFailedResponse("UNVERIFIED", "Email belum diverifikasi");
                return response()->json($data, 401);
            }

            if (!$token = Auth::attempt($credentials)) {
                $data = $this->createFailedResponse("WRONG_CREDENTIALS", "Email/Kata Sandi Anda salah");
                return response()->json($data, 401);
            }

            $user->api_token = $token;
            $user->save();
        } else {
            $data = $this->createFailedResponse("EMAIL_NOT_FOUND", "Email " . $request->email . " belum terdaftar");
            return response()->json($data, 401);
        }

        $data = $this->createSuccessJSON($user, "");
        return response()->json($data, 200);
    }

    public function loginByGoogle(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $fullname = $request->fullname;

        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            $data = $this->createFailedResponse("WRONG_CREDENTIALS", "Email/Kata Sandi Anda salah");
            return response()->json($data, 401);
        }

        $user = User::firstOrCreate(
            ['email' => $email],
            ['fullname' => $fullname, 'password' => $password]
        );

        $user->api_token = $token;
        $user->save();

        $data = $this->createSuccessJSON($user, "");
        return response()->json($data, 200);
    }

    public function register(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user != null) {
            if ($user->is_verified > 0) {
                $data = $this->createFailedResponse("ACCOUNT_VERIFIED", "Akun ini sudah diverifikasi. Silahkan Login");
                return response()->json($data, 403);
            } else {
                $data = $this->createFailedJSON("Akun Anda belum diverifikasi.");
                $this->sendVerificationCode($user->fullname, $user->notif_token, $user->verification_number);
                return response()->json($data, 401);
            }
        }

        $newuser = new User();
        $newuser->fullname = $request->fullname;
        $newuser->email = $request->email;
        $newuser->password = Hash::make($request->password);
        $newuser->birthdate = $request->birthdate;
        $newuser->gender = $request->gender;
        $newuser->notif_token = $request->notif_token;
        $newuser->verification_number = mt_rand(100000, 999999);
        $newuser->save();
        $data = $this->createSuccessJSON(null, "Berhasil daftar");

        $this->sendVerificationCode($request->fullname, $newuser->notif_token, $newuser->verification_number);
        return response()->json($data, 200);
    }

    public function resendVerificationCode(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user != null) {
            if ($user->is_verified > 0) {
                $data = $this->createFailedResponse("ACCOUNT_VERIFIED", "Akun ini sudah diverifikasi. Silahkan Login");
                return response()->json($data, 401);
            }

            $this->sendVerificationCode($user->fullname, $user->notif_token, $user->verification_number);
            $data = $this->createSuccessJSON("", "");
            return response()->json($data, 200);
        } else {
            $data = $this->createFailedResponse("EMAIL_NOT_FOUND", "Email " . $request->email . " belum terdaftar");
            return response()->json($data, 403);
        }
    }

    public function sendVerificationCode($name, $userToken, $numberCode)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder
            ->setTimeToLive(60 * 20)
            ->setPriority("high");

        $notificationBuilder = new PayloadNotificationBuilder('Kode Verifikasi Akun');
        $notificationBuilder
            ->setTag("ACCOUNT_VERIFICATION")
            ->setBody("Halo " . $name . ", Kode Verifikasi Anda adalah " . $numberCode)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['Code' => $numberCode]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $userToken;

//        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        FCM::sendTo($token, $option, $notification, $data);

        /*$downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();*/
    }

    public function sendEmail()
    {
        //send email
        Mail::to('yudantoanas@hotmail.com')->send(new Email());

    }

    public function verifyUser(Request $request)
    {
        $user = User::where([
            ['email', $request->email],
            ['verification_number', '=', $request->code]
        ])->first();

        if ($user != null) {
            if ($user->is_verified > 0) {
                $data = $this->createFailedResponse("ACCOUNT_VERIFIED", "Akun ini sudah diverifikasi. Silahkan Login");
                return response()->json($data, 403);
            }

            $user->is_verified = true;
            $user->verification_number = "";
            $user->save();

            $data = $this->createSuccessJSON(null, "Berhasil verifikasi akun");
            return response()->json($data, 200);
        } else {
            $data = $this->createFailedResponse("EMAIL_NOT_FOUND", "Email " . $request->email . " belum terdaftar");
            return response()->json($data, 403);
        }
    }

    public function logout()
    {
        $auth = Auth::user();

        Auth::logout();
        $user = User::where('email', $auth->email)->first();
        $user->api_token = null;
        $user->save();

        $data = $this->createSuccessJSON(null, "Berhasil Logout");
        return response()->json($data, 200);
    }
}
