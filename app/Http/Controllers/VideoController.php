<?php

namespace App\Http\Controllers;

use App\Category;
use App\Follower;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $limit = $request->get("limit");
        $sort = $request->get("sort");
        $filter = $request->get("filter");
        $category = $request->get("category");
        $query = $request->get("query");

        $videos = DB::table('videos')
            ->select(DB::raw("videos.id, title, video_url, video_thumbnail, description, published_at, videos.channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = videos.channel_id) AS channel,
            (SELECT channel_thumbnail FROM channels WHERE id = videos.channel_id) AS channel_thumbnail,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views"))
            ->leftJoin("blacklists", "videos.channel_id", "=", "blacklists.channel_id")
            ->whereNull("blacklists.channel_id");

        if ($category != null) {
            $videos->where("videos.category_id", Category::where("name", $category)->first()->id);
        }

        if (strlen($filter) > 0) {
            if (strtolower($filter) == "uploaded") $videos->where("is_published", 1);
            else if (strtolower($filter) == "drafted") $videos->where("is_published", 0);
        }

        if (strlen($sort) > 0) {
            if (strtolower($sort) == "created_at") $videos->orderBy($sort, "desc");
            else if (strtolower($sort) == "views") $videos->orderBy($sort, "desc");
        }

        if (strlen($query) > 0) {
            $videos->where("title", $query);
        }

        $pagedVideos = $videos->paginate($limit);
        $videoArray = array();
        foreach ($pagedVideos->toArray()["data"] as $video) {
            $follower = Follower::where("channel_id", "=", $video->channel_id)->first();
            if ($follower != null) {
                $video->follow_status = true;
            } else {
                $video->follow_status = false;
            }

            array_push($videoArray, $video);
        }

        $data = $this->createSuccessJSON(
            [
                'current_page' => $pagedVideos->currentPage(),
                'next_page' => $pagedVideos->nextPageUrl(),
                'prev_page' => $pagedVideos->previousPageUrl(),
                'videos' => $videoArray,
            ], "");

        return response()->json($data, 200);
    }

    public function getVideoFollow(Request $request)
    {
        $auth = Auth::user();
        $limit = $request->get("limit");

        $videos = DB::table('videos')
            ->select(DB::raw("videos.id, title, video_url, video_thumbnail, description, published_at, videos.channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = videos.channel_id) AS channel,
            (SELECT channel_thumbnail FROM channels WHERE id = videos.channel_id) AS channel_thumbnail,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views"))
            ->join('followers', 'videos.channel_id', '=', 'followers.channel_id')
            ->leftJoin("blacklists", "followers.channel_id", "=", "blacklists.channel_id")
            ->where("followers.user_id", $auth->id)
            ->whereNull("blacklists.channel_id")
            ->orderBy("published_at", "desc");

        $pagedVideos = $videos->paginate($limit);
        $videoArray = array();
        foreach ($pagedVideos->toArray()["data"] as $video) {
            $follower = Follower::where("channel_id", "=", $video->channel_id)->first();
            if ($follower != null) {
                $video->follow_status = true;
            } else {
                $video->follow_status = false;
            }

            array_push($videoArray, $video);
        }
        $data = $this->createSuccessJSON(
            [
                'current_page' => $pagedVideos->currentPage(),
                'next_page' => $pagedVideos->nextPageUrl(),
                'prev_page' => $pagedVideos->previousPageUrl(),
                'videos' => $videoArray,
            ], "");
        return response()->json($data, 200);
    }

    public function getVideo($id)
    {
        $video = DB::table("videos")
            ->select(DB::raw("id, title, video_url, video_thumbnail, description, published_at, channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = channel_id) AS channel,
            (SELECT channel_thumbnail FROM channels WHERE id = channel_id) AS channel_thumbnail,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views,
            (SELECT count(user_id) FROM followers WHERE channel_id = videos.channel_id) AS followers"))
            ->where("id", $id)
            ->first();

        $data = $this->createSuccessJSON($video, "");
        return response()->json($data, 200);
    }

    public function deleteVideo($id)
    {
        $count = Video::destroy($id);
        if ($count > 0) {
            $data = $this->createSuccessJSON("", "Berhasil menghapus video");
            return response()->json($data, 200);
        }

        $data = $this->createFailedJSON("Gagal menghapus video");
        return response()->json($data, 401);
    }

    public function changeStatus($id)
    {
        $video = Video::find($id);
        if ($video->is_published > 0) {
            $video->is_published = 0;
            $status = "draf";
        } else {
            $video->is_published = 1;
            $status = "publish";
        }
        $video->save();

        $data = $this->createSuccessJSON("", "Berhasil mengubah status menjadi " . $status);
        return response()->json($data, 401);
    }

    public function updateVideo(Request $request, $id)
    {
        $url = $request->video_url;

        $video = Video::find($id);
        $video->title = $request->title;
        $video->video_url = $request->video_url;
        $video->video_thumbnail = $this->getVideoThumbnail($url);
        $video->description = $request->description;
        $video->channel_id = $request->channel_id;
        $video->category_id = $request->category_id;
        $video->subcategory_id = $request->subcategory_id;
        $video->is_published = $request->is_published;
        $video->published_at = $request->published_at;
        $video->save();

        $data = $this->createSuccessJSON("", "Berhasil mengupdate video");
        return response()->json($data, 201);
    }

    private function getVideoThumbnail($url)
    {
        $videoID = $this->getVideoID($url);

        if (!$videoID) {
            $imageUrl = "https://img.youtube.com/vi/" . $videoID . "/maxresdefault.jpg";
            return $imageUrl;
        }

        return "";
    }

    private function getVideoID($url)
    {
        /**
         * Pattern matches
         * http://youtu.be/ID
         * http://www.youtube.com/embed/ID
         * http://www.youtube.com/watch?v=ID
         * http://www.youtube.com/?v=ID
         * http://www.youtube.com/v/ID
         * http://www.youtube.com/e/ID
         * http://www.youtube.com/user/username#p/u/11/ID
         * http://www.youtube.com/leogopal#p/c/playlistID/0/ID
         * http://www.youtube.com/watch?feature=player_embedded&v=ID
         * http://www.youtube.com/?feature=player_embedded&v=ID
         */
        $pattern =
            '%
            (?:youtube                    # Match any youtube url www or no www , https or no https
            (?:-nocookie)?\.com/          # allows for the nocookie version too.
            (?:[^/]+/.+/                  # Once we have that, find the slashes
            |(?:v|e(?:mbed)?)/|.*[?&]v=)  # Check if its a video or if embed
            |youtu\.be/)                  # Allow short URLs
            ([^"&?/ ]{11})                # Once its found check that its 11 chars.
            %i';

        // Checks if it matches a pattern and returns the value
        if (preg_match($pattern, $url, $match)) {
            return $match[1];
        }

        // if no match return false.
        return false;
    }

    public function createVideo(Request $request)
    {
        $url = $request->video_url;

        $video = new Video;
        $video->title = $request->title;
        $video->video_url = $request->video_url;
        $video->video_thumbnail = $this->getVideoThumbnail($url);
        $video->description = $request->description;
        $video->channel_id = $request->channel_id;
        $video->category_id = $request->category_id;
        $video->subcategory_id = $request->subcategory_id;
        $video->is_published = $request->is_published;
        $video->published_at = $request->published_at;
        $video->save();

        $data = $this->createSuccessJSON("", "Berhasil membuat video");
        return response()->json($data, 201);
    }
}
