<?php

namespace App\Http\Controllers;

use App\Follower;
use App\LaterVideo;
use App\Playlist;
use App\PlaylistVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlaylistController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        $auth = Auth::user();

        $playlists = Playlist::where('user_id', '=', $auth->id)->get();

        $playlistArray = array();
        foreach ($playlists as $playlist) {
            $playlist->video_count = PlaylistVideo::where("playlist_id", $playlist->id)->count();

            array_push($playlistArray, $playlist);
        }

        $data = $this->createSuccessJSON($playlists->toArray(), "");
        return response()->json($data, 200);
    }

    public function watchLater()
    {
        $auth = Auth::user();

        $videos = DB::table("videos")
            ->select(DB::raw("videos.id, title, video_url, video_thumbnail, description, published_at, videos.channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = videos.channel_id) AS channel,
            (SELECT channel_thumbnail FROM channels WHERE id = videos.channel_id) AS channel_thumbnail,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views"))
            ->join('later_videos', 'video_id', '=', "videos.id")
            ->where('user_id', '=', $auth->id)
            ->get();

        $videoArray = array();
        foreach ($videos->toArray() as $video) {
            $follower = Follower::where("channel_id", "=", $video->channel_id)->first();
            if ($follower != null) {
                $video->follow_status = true;
            } else {
                $video->follow_status = false;
            }

            array_push($videoArray, $video);
        }
        $data = $this->createSuccessJSON($videoArray, "");
        return response()->json($data, 200);
    }

    public function createPlayList(Request $request)
    {
        $auth = Auth::user();

        $playlist = Playlist::firstOrCreate(
            ["name" => $request->name],
            ["user_id" => $auth->id]
        );

        PlaylistVideo::firstOrCreate(["playlist_id" => $playlist->id, "video_id" => $request->video_id]);

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function addToPlaylist(Request $request, $id)
    {
        PlaylistVideo::firstOrCreate(["playlist_id" => $request->playlist_id, "video_id" => $id]);

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function removeFromPlaylist(Request $request, $id)
    {
        PlaylistVideo::where([
            ["video_id", $id],
            ["playlist_id", $request->playlist_id]
        ])->delete();

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function addToWatchLater($id)
    {
        $auth = Auth::user();

        LaterVideo::firstOrCreate(["user_id" => $auth->id, "video_id" => $id]);

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function deleteFromWatchLater($id)
    {
        $auth = Auth::user();

        LaterVideo::where(["user_id" => $auth->id, "video_id" => $id])->delete();

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function getPlaylist($id)
    {
        $playlist = Playlist::find($id);

        $videos = DB::table("videos")
            ->select(DB::raw("videos.id, title, video_url, video_thumbnail, description, published_at, videos.channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = videos.channel_id) AS channel,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views"))
            ->join("playlist_videos", "video_id", "=", "videos.id")
            ->where("playlist_id", "=", $id)
            ->get();

        $playlist->videos = $videos;

        $data = $this->createSuccessJSON($playlist, "");
        return response()->json($data, 200);
    }

    public function changePlaylistName(Request $request, $id)
    {
        $playlist = Playlist::find($id);
        $playlist->name = $request->name;
        $playlist->save();

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function deletePlaylist($id)
    {
        Playlist::destroy($id);

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }
}
