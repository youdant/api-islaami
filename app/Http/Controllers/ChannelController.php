<?php

namespace App\Http\Controllers;

use App\Blacklist;
use App\Channel;
use App\Follower;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChannelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $limit = $request->get("limit");
        $sort = $request->get("sort");
        $filter = $request->get("filter");

        $channels = DB::table('channels')
            ->select(DB::raw("channels.id, name, channel_thumbnail,
            (SELECT count(id) FROM videos WHERE channel_id = channels.id) AS videos"));

        if (strlen($filter) > 0) {
            if (strtolower($filter) == "active") $channels->where("is_suspended", 0);
            else if (strtolower($filter) == "inactive") $channels->where("is_suspended", 1);
        }

        if (strlen($sort) > 0) {
            if (strtolower($sort) == "created_at") $channels->orderBy($sort, "desc");
            else if (strtolower($sort) == "videos") $channels->orderBy($sort, "desc");
        }

        $data = $this->createSuccessJSON($channels->paginate($limit), "");
        return response()->json($data, 200);
    }

    public function getChannelDetail($id)
    {
        $channel = DB::table('channels')
            ->select(DB::raw('id, name, channel_thumbnail, channel_bio, is_suspended,
            (SELECT COUNT(*) FROM followers WHERE followers.channel_id = channels.id) as followers'))
            ->where("channels.id", $id)
            ->first();

        $data = $this->createSuccessJSON($channel, "");
        return response()->json($data, 200);
    }

    public function getChannelVideos($id)
    {
        $videos = DB::table('videos')
            ->select(DB::raw("videos.id, title, video_url, video_thumbnail, description, published_at, videos.channel_id, category_id, subcategory_id,
            (SELECT name FROM channels WHERE id = videos.channel_id) AS channel,
            (SELECT channel_thumbnail FROM channels WHERE id = videos.channel_id) AS channel_thumbnail,
            (SELECT name FROM categories WHERE id = category_id) AS category,
            (SELECT name FROM subcategories WHERE id = subcategory_id) AS subcategory,
            (SELECT count(distinct user_id) FROM video_views WHERE video_id = videos.id) AS views"))
            ->where("channel_id", $id)
            ->orderBy("published_at", "desc")
            ->get();

        $data = $this->createSuccessJSON($videos, "");
        return response()->json($data, 200);
    }

    public function getChannelHide(Request $request)
    {
        $auth = Auth::user();

        $channels = DB::table('channels')
            ->select(DB::raw("channels.id, name, channel_thumbnail,
            (SELECT count(id) FROM videos WHERE videos.channel_id = channels.id) AS videos"))
            ->join("blacklists", "channels.id", "=", "blacklists.channel_id")
            ->where("user_id", $auth->id)
            ->get();

        $data = $this->createSuccessJSON($channels, "");
        return response()->json($data, 200);
    }

    public function getChannelFollow(Request $request)
    {
        $auth = Auth::user();

        $channels = DB::table('channels')
            ->select(DB::raw("channels.id, name, channel_thumbnail,
            (SELECT count(id) FROM videos WHERE videos.channel_id = channels.id) AS videos"))
            ->join("followers", "channels.id", "=", "followers.channel_id")
            ->leftJoin("blacklists", "channels.id", "=", "blacklists.channel_id")
            ->where("followers.user_id", $auth->id)
            ->whereNull("blacklists.channel_id")
            ->get();

        $data = $this->createSuccessJSON($channels, "");
        return response()->json($data, 200);
    }

    public function createChannel(Request $request)
    {
        return response()->json(Channel::create($request->all()), 201);
    }

    public function getFollowingStatus($id)
    {
        $auth = Auth::user();

        $follower = Follower::where([
            ['user_id', "=", $auth->id],
            ['channel_id', "=", $id]
        ])->first();

        $customData = array(
            'isFollow' => false
        );

        if ($follower != null) {
            $customData["isFollow"] = true;
        }

        $data = $this->createSuccessJSON($customData, "");

        return response()->json($data, 200);
    }

    public function getHideStatus($id)
    {
        $auth = Auth::user();

        $blacklist = Blacklist::where([
            ['user_id', "=", $auth->id],
            ['channel_id', "=", $id]
        ])->first();

        $customData = array(
            'isHidden' => false
        );

        if ($blacklist != null) {
            $customData['isHidden'] = true;
        }

        $data = $this->createSuccessJSON($customData, "");

        return response()->json($data, 200);
    }

    public function followChannel($id)
    {
        $auth = Auth::user();

        Follower::firstOrCreate(
            ["user_id" => $auth->id, "channel_id" => $id]
        );

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function unfollowChannel($id)
    {
        $auth = Auth::user();

        Follower::where([
            ['user_id', "=", $auth->id],
            ['channel_id', "=", $id]
        ])->delete();

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function hideChannel($id)
    {
        $auth = Auth::user();

        Blacklist::firstOrCreate(
            ["user_id" => $auth->id, "channel_id" => $id]
        );

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }

    public function showChannel($id)
    {
        $auth = Auth::user();

        Blacklist::where([
            ['user_id', "=", $auth->id],
            ['channel_id', "=", $id]
        ])->delete();

        $data = $this->createSuccessJSON(null, "");
        return response()->json($data, 200);
    }
}
