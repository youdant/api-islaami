<?php

namespace App\Http\Controllers;

use App\Category;
use App\Label;
use App\Subcategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $categories = Category::all();
        $data = $this->createSuccessJSON($categories, "");
        return response()->json($data, 200);
    }

    public function subcategoryIndex()
    {
        $subcategories = Subcategory::all();
        return $subcategories;
    }

    public function labelIndex()
    {
        $labels = Label::all();
        return $labels;
    }

    public function createCategory(Request $request)
    {
        if ($request->order_number != 0) {
            $category = Category::where("order_number", $request->order_number)->first();
            if (strlen($category) > 0) {
                $data = $this->createFailedJSON("order number " . $request->order_number . " already exist, please use next number");
            } else {
                Category::create($request->all());
                $data = $this->createSuccessJSON(null, "");
            }
        } else {
            $data = $this->createFailedJSON("order number must not be 0 (zero) or below");
        }

        return response()->json($data, 201);
    }

    public function createSubcategory(Request $request)
    {
        return response()->json(Subcategory::create($request->all()), 201);
    }
}
