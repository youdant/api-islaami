<?php

namespace App\Http\Controllers;

use App\User;
use App\UserInsight;
use App\UserRecommendation;
use App\UserReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $limit = $request->get("limit");
//        $filter = $request->get("filter");

        $reports = DB::table('user_reports');

        if (strlen($limit) == 0) $limit = 10;

        $data = $this->createSuccessJSON($reports->paginate($limit), "");
        return response()->json($data, 200);
    }

    public function createReport(Request $request)
    {
        $auth = Auth::user();

        UserReport::create([
            'username'=>$auth->fullname,
            'description'=>$request->description,
            'image_url'=>$request->image_url,
        ]);

        $data = $this->createSuccessJSON("", "SUCCESS");
        return response()->json($data, 200);
    }

    public function changeReportStatus($id)
    {
        $userReport = UserReport::find($id);
        if ($userReport->is_solved == 0) {
            $userReport->is_solved = 1;
            $userReport->save();
            $data = $this->createSuccessJSON("", "Laporan Berhasil Ditanggapi");
        } else {
            $data = $this->createFailedJSON("Laporan Sudah Ditanggapi");
        }

        return response()->json($data, 200);
    }

    // INSIGHTS
    public function indexInsight(Request $request)
    {
        $limit = $request->get("limit");
//        $filter = $request->get("filter");

        $insights = DB::table('user_insights');

        if (strlen($limit) == 0) $limit = 10;

        $data = $this->createSuccessJSON($insights->paginate($limit), "");
        return response()->json($data, 200);
    }

    public function createInsight(Request $request)
    {
        $auth = Auth::user();

        UserInsight::create([
            'username'=>$auth->fullname,
            'detail'=>$request->detail
        ]);

        $data = $this->createSuccessJSON("", "SUCCESS");
        return response()->json($data, 200);
    }

    // RECOMMENDATION
    public function indexRecommendation(Request $request)
    {
        $limit = $request->get("limit");
//        $filter = $request->get("filter");

        $recommendations = DB::table('user_recommendations');

        if (strlen($limit) == 0) $limit = 10;

        $data = $this->createSuccessJSON($recommendations->paginate($limit), "");
        return response()->json($data, 200);
    }

    public function createRecommendation(Request $request)
    {
        $auth = Auth::user();

        UserRecommendation::create([
            'username'=>$auth->fullname,
            'channel_name'=>$request->channel_name,
            'channel_url'=>$request->channel_url,
        ]);

        $data = $this->createSuccessJSON("", "SUCCESS");
        return response()->json($data, 200);
    }

    private function getUsername($id)
    {
        $user = User::find($id);

        return $user->fullname;
    }
}
