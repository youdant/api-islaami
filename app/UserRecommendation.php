<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRecommendation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'channel_name',
        'channel_url',
    ];
}
