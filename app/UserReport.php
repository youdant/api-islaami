<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'description',
        'image_url',
        'is_solved',
    ];
}
