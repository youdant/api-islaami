<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'video_url',
        'video_thumbnail',
        'description',
        'channel_id',
        'category_id',
        'subcategory_id',
        'is_published',
        'published_at',
    ];

    public function channel()
    {
        return $this->belongsTo('App\Channel');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    /*public function videoLabels()
    {
        return $this->hasMany('App\VideoLabel');
    }*/
}
